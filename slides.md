<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->

## Architecture - The Hard Thing
<!-- .element: class="no-toc-progress" --> <!-- slide not in toc progress bar -->

----

### Start with a Quote ;)

_"There is not best solution. There are only trade-offs you can live with and those you can't."_

-- no idea who said that

----  ----

## Why Architecture?

_"There is no single development, in either technology or management technique, which by itself promises even one order of magnitude improvement within a decade in productivity, in reliability, in simplicity"_

- Fred Brooks from "No Silver Bullet"

Note:

In SD we often become experts in 'the best' solution to a problem.
In Architecture we recognize that there is no globally best solution,
just a least-worst combination of trade-offs. That is what we strive for
when practising Architecture.

----

## On Data

- _Data is a precious thing and will last longer than the systems themselves._
- Essentially 2 types
  - Operational Data: You know the stuff we CRUD on. Online Transactional Processing (OLTP).
  - Analytical Data: The stuff we base long-term decisions on. Online Analytical Processing (OLAP). Often less 'structured'

Note:
Quote by Tim Berners-Lee. Say usual things about quotes.


----  ----

## Essential Ideas

- Quanta
- Dimensions
- Sagas

----

### Architecture Quanta

- Independently deployable artifact
- High functional cohesion
- High static coupling
- Synchronous dynamic coupling

Note:

- Independent deployability is useful for many things, but failure domains still depend on other factors.
- Cohesion: Refers to the proximity of related elements (components/services/etc.) overlaps with DDD ideas.
- Static coupling: Represents how static dependencies like frameworks, libraries, operating systems etc. are delivered.
- Dynamic coupling: Represents how quanta communicate at runtime. I.e. how things 'call' each other at runtime.

----

### Dimensions

- Communication
  - synchronous (S)
  - asynchronous (A)
- Consistency
  - atomic (A)
  - eventual (E)
- Coordination
  - orchestrated (O)
  - choreographed (C)

Note:
Sometimes coupling is included in this table but I think that that is more or less emergent from the combination of other properties.
So I choose to exclude it here. Reasonable people might disagree, though.

----

### Sagas

- Epic (SAO)
- Phone Tag (SAC)
- Fairy Tale (SEO)
- Time Travel (SEC)
- Fantasy Fiction (AAO)
- Horror Story (AAC)
- Parallel (AEO)
- Anthology (AEC)

Note:

Each of those would fill pages. I just name drop them here so you have
a starting point for your own reading.

----  ----

## Pulling things apart

- Discerning Coupling
- Modularity Drivers
- Component-Based Decomposition
- Data Decomposition
- On Granularity

----

### Discerning Coupling

- Coupling: Two parts are encoupled if a change in one might cause a change in the other
- For trade-off analysis:
  - Find what parts are entangled together
  - Analyze how they are coupled to one another
  - Assess trade-offs by determining the impact of change on interdependent systems
- Think: Quanta, Independent deployability, Cohesion, Dynamic/Static Quantum Coupling

----

### Modularity Drivers

- Speed-to-Market
- Maintainability
- Testability
- Deployability
- Scalability (vs. Elasticity)
- Availability/Fault Tolerance

Note:

Scaling goes in one direction, elasticity is a yoyo.
Keep in mind, that modularity (in the sense of services) is not always justified/justifiable

----

### Component-Based Decomposition

- Basically just name dropping this
- Many tactical patterns
- e.g. Tactical Forking, Flatten Components,...
- Key Term: Fitness Function

Note:

A fitness function determines whether or not an architectural property is satisfied,
sometimes in terms of to what degree. Thing of them like tests for architecture, yes
they can run in your CI ;)

----

### Data Decomposition

- Everything that writes to the same database is the same quantum!
- Disintegrators i.e. drivers for splits:
  - Change Control
  - Connection Management
  - Scalability
  - Fault tolerance
  - Quantum Considerations
  - Database type considerations

Note:

Again mostly name drops for your own research

----

### On Granularity

- Modularity: Constructed with standardized units or dimensions for flexibility and variety in use
- Granularity: Consisting of or appearing to consist of one of numerous particles forming a larger unit
- (Dis-)integrators:
  - Scope and function
  - Code Volatility
  - Scalability/Throughput
  - Fault Tolerance
  - Security
  - Extensibility

Note:

Often (incorrectly) used interchangeably.

----  ----

## Putting things together

- Reuse Patterns
- Data Ownership/Distributed Transactions
- Distributed Data Access
- Distributed Workflows
- Contracts

Note:

We will not talk about distributed transaction patterns today.
That is a topic that has more depth than could possibly fit
into a short intro. For now, just be aware that it exists.

----

### Reuse Patterns

- Code Replication: Utilities, Things unlikely to change, Thinks that evolve independently
- Shared Library: Homogeneous environments, low rate of change
- Shared Service: Highly polyglot environments, high rate of change
- Sidecar/Service Mesh: Cross-Cutting Concerns, think decorators

Note:

As always, mind the trade offs

----

### Data Ownership/Distributed Transactions

- Single Ownership: That one is easy
- Joint Ownership:
  - Table Split: Not always possible
  - Data Domain: Not always desireable
  - Delegate: Not always feasible
  - Service Consolidation: Not always "clean"

Note:

Data Domain has many issues in DDD/SoA contexts.
Delegate can have service interdependencies that resemble get/set RPC

----

### Distributed Data Access

- Inter-Service Calls
- Column Schema Replication
- Replicated Caching
- Data Domain

----

### Distributed Workflows

- Orchestration
  - Centralized Flow with error handling, recoverabilty and state management
  - Limited fault tolerance, scalability and responsiveness with high coupling
- Choreography
  - Multiple patterns: Stamping, Stateless, Front Controller
  - Responsive, scalable, decoupled and fault tolerant
  - Distributed Workflow, poor error handling, poor recoverability, state management

----

### Contracts

- A written agreement between services
- Strict: XML Schema, JSON Schema, Object, RPC
- Intermediate: JSON, GraphQL
- Loose: Value-driven contracts, Simple JSON, KVP arrays(maps)
- Stricter contracts increase coupling but provide fidelity and documentation
- Looser contracts reduce coupling at the cost of no fidelity or 'contract' out of the box

----  ----

## Trade-Off Analysis

- Entangled Dimensions
- Trade-Off Techniques
  - Qualitative vs Quantitative
  - MECE Lists
  - Modelling Relevant Domain Cases
  - Bottom Line over Overwhelming Evidence
  - No Snake Oil

----

### Core Workflow

- Find what parts are entangled together
- Analyze how they are coupled to one another
- Assess trade-offs by determining the impact of change to interdependent systems

----

### Entangled Dimensions

- Coupling
  - Communication
  - Consistency
  - Coordination
- Static/Dynamic Dependencies
- Integration Points

----

### Trade-Off Techniques

Once you have all the info together to start asking 'what if' type questions you can use
the following to get to an analysis.

----

### Qualitative vs Quantitative

- True quantitative assessments are rarely possible
- Building comparative scales for the properties of interest along the implementation options usually helps
  - example: Assessing _scalability_ along different combinations of communication, consistency and coordination

----

### MECE Lists

- Mutually Exclusive: No overlap between compared items
- Collectively Exhaustive: Cover the whole decision space
- Compare and select

----

### Modelling Relevant Domain Cases

- As the name suggests, pick a few (the most valuable) domain cases
- Model them in different architectures
- Also play around with extending the functionality (e.g. add another payment type)
- Document and draw conclusions

Note:

You can read this as 'use cases' or 'business cases' at the end it means,
"the thing that makes you money' or more philosophically, you raison d'etre.

----

### Bottom Line over Overwhelming Evidence

Usually when doing analysis it is easy to gather and generate enormous amounts of information.

Keep in mind that most of this is arcane at best and boring at worst to most (non-)technical
stakeholders.

Reduce your trade-off analysis to a few key-points before putting it out there.
If it is more than one A4 page (including images) it is likely to much ;)

----

### No Snake Oil

Resist the urge to jump onto the band wagon of the latest, shiniest, all-problems solving
silver bullet.

The trade-offs will always eventually come back to bite you, no matter how small you talk them
to get something through the door ;)

----  ----

## Thank you
